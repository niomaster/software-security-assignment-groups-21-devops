#!/usr/bin/env python3

from flask import Flask
from flask import render_template
from flask import request
from flask import Markup
from flask import escape

import math

app = Flask(__name__)

def analyzeNumber(n):
    try:
        n = int(n)
        r = "Analysis for <b>{}</b>:".format(n)
        if (n % 2 == 0):
            r = r + " The number is even."
        else:
            r = r + " The number is odd."
        r = r +  " The square root is: {}".format(math.sqrt(n))
        return Markup(r)
    except:
        return Markup("Sorry, failed to parse {}".format(n))

@app.route('/', methods=['GET'])
def hello_world():
    number = None
    analysis = None
    try:
        number = escape(request.args['number'])
        analysis = analyzeNumber(number)
        # analysis = "Test"
    except:
        pass
    return render_template('index.html', appName='Number Information', number=number, analysis=analysis)
