### Number analyzer

A simple number analyzer written in Python and flask

### Gitlab Auto DevOps

This project is Auto DevOps compatible. However you need to set the environment variable _TEST_DISABLED_ in your project settings. Otherwise the test stage will fail since the current Auto DevOps setup of gitlab doesn't support tests for Python.
